#!/bin/sh


# Helper functions

dirChecker () {
        if ! test -d "$1"
        then
                mkdir -p $1
        else
                rm -rf $1/*
        fi
}


# Install repositories and software

sudo add-apt-repository ppa:mmstick76/alacritty -y
sudo apt update 
sudo apt install \
	alacritty \
	firefox \
	galculator \
	gimp \
	gnome-tweaks \
	htop \
	inkscape \
	keepassxc\
	libghc-xmonad-contrib-dev \
	libghc-xmonad-dev \
	libreoffice \
	neofetch \
	nextcloud-desktop \
	nitrogen \
	picom \
	pcmanfm \
	qt5ct \
	rofi \
	snap \
	stalonetray \
	stow \
	sxiv \
	thunderbird \
	vim \
	virtualbox \
	xdotool \
	xmobar \
	xmonad \
	yad \
	zathura \
	-y

sudo apt remove
	gedit \
	gnome-terminal \
	nautilus \
	snap \
	snapd \
	-y

sudo apt autoremove -y

# Set GTK Theme

wget -P $HOME/Downloads "https://github.com/dracula/gtk/archive/master.zip"
sudo unzip $HOME/Downloads/master.zip -d /usr/share/themes/
sudo mv /usr/share/themes/gtk-master /usr/share/themes/Dracula
gsettings set org.gnome.desktop.interface gtk-theme "Dracula"
gsettings set org.gnome.desktop.wm.preferences theme "Dracula"
sudo rm -rf $HOME/Downloads/master.zip

# Set GTK Icons

wget -P $HOME/Downloads "https://github.com/dracula/gtk/files/5214870/Dracula.zip"
sudo unzip $HOME/Downloads/Dracula.zip -d /usr/share/icons/
gsettings set org.gnome.desktop.interface icon-theme "Dracula"
sudo rm -rf $HOME/Downloads/Dracula.zip

# Set QT5 Theme

echo "Downloading and installing Dracula QT5-Theme"

wget -P $HOME/Downloads "https://github.com/dracula/qt5/archive/master.zip"
dirChecker "$HOME/Downloads/draculaqt5" 

sudo unzip $HOME/Downloads/master.zip -d $HOME/Downloads/draculaqt5
dirChecker "$HOME/.config/qt5ct/colors"
sudo mv $HOME/Downloads/draculaqt5/qt5-master/Dracula.conf $HOME/.config/qt5ct/colors
sudo rm -rf $HOME/Downloads/draculaqt5 $HOME/Downloads/master.zip

# Download Dracula Theme for Alacritty and install

echo "Downloading Dracula Theme for Alacritty"
echo "Don't forget to change the path to the file in the alacritty config $HOME/.config/alacritty/alacritty.yml to $HOME/.git/dracula/alacritty/dracula.yml"

dirChecker "$HOME/.git/dracula/alacritty"
rm -rf $HOME/.git/dracula/alacritty
git clone https://github.com/dracula/alacritty.git $HOME/.git/dracula/alacritty




# Download Dotfiles and replace
dirChecker "$HOME/.dotfiles"
rm -rf $HOME/.dotfiles
git clone https://gitlab.com/trikap/.dotfiles.git $HOME/.dotfiles
stow . --adopt -d $HOME/.dotfiles
